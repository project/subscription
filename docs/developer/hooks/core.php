<?php

/**
 * @file
 * These are the hooks that are invoked by the Drupal subscription package.
 *
 * These hooks are typically called in all modules at once using
 * module_invoke_all().
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Provide objects to which users may subscribe, or provide a channel
 * for delivering notifications.
 *
 * @param $op
 *   What kind of action is being performed. Possible values:
 *   - "channel": Provide a new mechanism for notifying users. For example, email, instant message, sky writing ...
 *   - "formatter": 
 *   - "cron": Notify non-instant users. In this case, $op1 is time of last cron and $op2 is current cron time.
 *   - "special": Injewct a link into the My Subscriptions page offerring a new, special subscription.
 * @param $arg1
 *   - Optional. Used in 'cron' op. See above.
 * @param $arg2
 *   - Optional. Used in 'cron' op. See above.
 * @return
 *   This varies depending on the operation.
 *   - channel: an array of arrays whre the inner arrays have keys: 'name' (a human readable name), 'id' (a brief string without spaces), 'handler' (a function which will be called back).
 *   - formatter: an array of arrays where the inner arrays have keys: 'name', 'object' (TODO), 'channel' (TODO), 'weight', 'handler' 'handler' (a function which will be called back).  
 *   - cron: an array of objects. objects should have a id element. exaples are $node and $comment 
 *   - special: HTML, usually just a link./ will be displayed within an unordered list.
 */
function hook_subscription($op, $arg1 = null, $arg2 = null) {
  switch ($op) {
  case 'channel':
    $channels = array();
    $channels[] = array('name' => t('Debugger'), 'id' => 'debug', 'handler' => 'subscription_debuggerchannel');
    $channels[] = array('name' => t('Simple mailer'), 'id' => 'simple_mail', 'handler' => 'subscription_simplemailchannel');
    return $channels;
  case 'formatter':
    $formatters = array();
    return $formatters;
  case 'cron':
    if (!variable_get('subscription_instant_mode', 0)) {
      return array();
    }
    $new_objects = array();
    $result = db_query(db_rewrite_sql("SELECT n.*, u.uid, u.name, u.picture, u.data FROM {node} n INNER JOIN {users} u ON u.uid = n.uid WHERE n.created>'%d' AND n.created<='%d'", $arg1, $arg2));
    $nodes = array();
    while ($node = db_fetch_object($result)) {
      $node = drupal_unpack($node);
      $node->date = format_date($node->created, 'medium');
      $nodes[] = $node;
    }
    $new_objects['node'] = $nodes;
    
    $result = db_query(db_rewrite_sql("SELECT c.*, u.uid, u.name, u.picture, u.data FROM {comments} c INNER JOIN {users} u ON u.uid = c.uid WHERE c.timestamp>'%d' AND c.timestamp<='%d'", $arg1, $arg2));
    $comments = array();
    while ($comment = db_fetch_object($result)) {
      $comment = drupal_unpack($comment);
      $comment->date = format_date($comment->timestamp, 'medium');
      $comments[] = $comment;
    }
    $new_objects['comment'] = $comments;
    return array($new_objects);
  case 'special':
      return l(t("Subscribe to all comments"), 'subscription/add/comment');
  }
}


/**
 * @} End of "addtogroup hooks".
 */

?>
