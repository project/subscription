CREATE TABLE subscription_channels (
  cid int default '0',
  uid smallint default '0',
  name text default NULL,
  id text default NULL,
  daily int default '0'
);

INSERT INTO subscription_channels (cid, uid, name, id, daily) VALUES(2, 0, 'Instant mailer', 'simple_mail', 0);
INSERT INTO sequences (name, id) VALUES ("subscription_channels",3);

CREATE TABLE subscription_subscriptions (
  sid int primary key NOT NULL default '0',
  uid int NOT NULL default '0',
  condition text NOT NULL,
  cid int default '0',
  object text NOT NULL default ''
);

--grant all on subscription_channels to apache;
--grant all on subscription_subscriptions to apache;

