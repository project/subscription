subscription.module README
==============================================================================

Installation, Configuration, Administration
------------------------------------------------------------------------------

see INSTALL.txt

Credits / Contact
------------------------------------------------------------------------------

The original author of this module is Elek, Márton, who can be reached at
einstand - at[deletethis] - anzix - net.

Some code is reused from Kjartan Mannes: Notify, Dan Ziemecki: Subscriptions,
and Robert Douglas: sandbox/subscriptions modules 

Thx to Károly Négyesi (chx) my mentor in Google SoC program.

Change Log
------------------------------------------------------------------------------

08/28/2005 (ee)
---------
- initial cvs release
